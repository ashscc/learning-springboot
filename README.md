# learning-springboot
学习springboot加其他中间件或框架等的一些demo


## 项目介绍
> 1. 这是一个学习springboot加其他框架或者中间件等开发的一些demo
> 2. 目前包括
1. nacos+dubbo远程调用的demo
2. prometheus+pushgateway+grafana提供app推送指标到pushgateway，再由pushgateway推送到prometheus的过程的demo，以及直接由app推送到prometheus的demo
3. rocketmq的几种推送方式
4. 权限框架shiro的使用
5. 银行还在用的webservice方式开发
6. SpringBoot的@Validated标签和@Valid的使用及区别 [文档传送门](https://www.cnblogs.com/ashScc/p/16414649.html)
>
---
### 开发日志[根据版本提交并介绍版本改动]
1. [master] &nbsp; 版本v1.0.0  &nbsp;时间:```起:??? 止:2021-3-24```
    1.  开源第一版，里面都是经过我测试过的，都是可以用的，服务都是通过docker起镜像的服务实现的，超过1000star后面更新就附带整个过程的md文档
  