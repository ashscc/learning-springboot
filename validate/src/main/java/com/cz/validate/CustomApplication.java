package com.cz.validate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = {"com.cz.validate"})
@EnableSwagger2
@Slf4j
public class CustomApplication {

    @Bean
    public ApplicationRunner runner() {
        return args -> log.info("started success!");
    }

    public static void main(String[] args) {
        SpringApplication.run(CustomApplication.class, args);
    }


}
