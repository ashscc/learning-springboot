package com.cz.validate.common;

public interface StatusCode {
    public int getCode();
    public String getMsg();
}