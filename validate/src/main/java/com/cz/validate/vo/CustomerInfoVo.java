package com.cz.validate.vo;

import com.cz.validate.groups.AddUser;
import com.cz.validate.groups.EditUser;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @Description todo
 * @Author cz
 * @Date 2022/6/24
 */
@Data
public class CustomerInfoVo {
    //在分组EditUser时，验证id不能为空，其他情况下不做验证
    private Long id;

    @NotEmpty(message = "用户名不能为空")
    private String username;

    @Size(min=6 ,max= 20 , message = "密码最少6位，最高20位")
    private String password;

    //此处不填分组消息表示所有分组都需要校验
    @NotNull
    @Valid
    private UserIdCardVo userIdCardVo;
}
