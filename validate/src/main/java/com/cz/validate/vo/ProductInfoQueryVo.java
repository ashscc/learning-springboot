package com.cz.validate.vo;

import com.cz.validate.groups.AddUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

/**
 * @Description todo
 * @Author cz
 * @Date 2022/6/23
 */
@Data
public class ProductInfoQueryVo {

    // 商品名称
    @NotBlank(message = "商品名称不能为空")
    @ApiModelProperty(value = "商品名称", required = true)
    private String productName;
    // 商品价格
    @Range(min = 0, message = "商品价格不能为负数")
    @ApiModelProperty(value = "商品价格", required = true)
    private BigDecimal productPrice;
}
