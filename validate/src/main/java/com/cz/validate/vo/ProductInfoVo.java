package com.cz.validate.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfoVo {

    //商品id
    private Long productId;
    // 商品名称
    private String productName;
    //价格
    private BigDecimal productPrice;
    //描述
    private String productDescription;
}