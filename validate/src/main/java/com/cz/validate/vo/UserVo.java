package com.cz.validate.vo;

import com.cz.validate.groups.AddUser;
import com.cz.validate.groups.EditUser;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserVo {

    //在分组EditUser时，验证id不能为空，其他情况下不做验证
    @NotNull(groups={EditUser.class})
    private Long id;

    @NotEmpty(message = "用户名不能为空", groups = {AddUser.class, EditUser.class})
    private String username;

    @Size(min=6 ,max= 20 , message = "密码最少6位，最高20位", groups = {AddUser.class, EditUser.class})
    private String password;

    //此处不填分组消息表示所有分组都需要校验
    @NotNull(groups = {AddUser.class, EditUser.class})
    private UserIdCardVo userIdCardVo;
}