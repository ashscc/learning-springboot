package com.cz.validate.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CustomerIdCardVo {

    @NotEmpty(message = "用户名不能为空")
    private String username;

    @Size(min = 18, max = 18, message = "身份证号码应该为18位")
    private String identity;
}