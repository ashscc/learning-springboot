package com.cz.validate.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description todo
 * @Author cz
 * @Date 2022/6/23
 */
@Data
public class ProductInfo {
    //商品id
    private Long productId;
    // 商品名称
    private String productName;
    //价格
    private BigDecimal productPrice;
    //描述
    private String productDescription;
    // 上架状态
    private Integer productStatus;
}
