package com.cz.validate.model;

import lombok.Data;

@Data
public class User {
    //用户id
    private String id;
    //用户名称
    private String username;
    //用户密码
    private String password;
}