package com.cz.validate.controller;

import com.cz.validate.common.ResultCode;
import com.cz.validate.exception.APIException;
import com.cz.validate.vo.CustomerInfoVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Description todo
 * @Author cz
 * @Date 2022/6/26
 */
@RestController
@RequestMapping("/customer/v1")
public class CustomerInfoController {


    /**
     * 对于@Valid
     * 由于不支持分组，所以对于所以校验标签里面添加了groups字段的都不会校验会跳过
     * `@Valid` 加不加@RequestBody 基本校验会进行，嵌套校验需要在嵌套的对象上面加@Valid则可以进行嵌套校验
     * 由于没有分组的概念，对于不同情况校验字段不同则需要在业务逻辑里面判断
     *
     * @param vo     用户
     * @param result 接收异常
     * @return success
     */
    @PostMapping("/add")
    public String addCustomer(@RequestBody @Valid CustomerInfoVo vo, BindingResult result) throws APIException, JsonProcessingException {
        //测试全局拦截器
        if (null == vo) {
            throw new APIException(ResultCode.FAILED, "用户信息为空!");
        }
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(result.toString());
        System.out.println(vo);
        if (result.hasErrors()) {
            StringBuilder sb = new StringBuilder();
            for (ObjectError error : result.getAllErrors()) {
                System.out.println(mapper.writeValueAsString("捕获到的单个error对象:" + error));
                System.out.println(error.getDefaultMessage());
                sb.append(error.getDefaultMessage()).append(",");
            }
            throw new APIException(ResultCode.FAILED, sb.toString());
        }
        return "success";
    }

    /**
     * 测试分组校验中，对于修改用户需要校验其对应的id存在，才能去修改对应id用户的账号密码
     *
     * @param vo     用户
     * @param result 接收异常
     * @return success
     */
    @PostMapping("/edit")
    public String editCustomer(@RequestBody @Valid CustomerInfoVo vo, BindingResult result) throws APIException, JsonProcessingException {
        //测试全局拦截器
        if (null == vo) {
            throw new APIException(ResultCode.FAILED, "用户信息为空!");
        }
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(result.toString());
        System.out.println(vo);
        if (result.hasErrors()) {
            StringBuilder sb = new StringBuilder();
            for (ObjectError error : result.getAllErrors()) {
                System.out.println(mapper.writeValueAsString("捕获到的单个error对象:" + error));
                System.out.println(error.getDefaultMessage());
                sb.append(error.getDefaultMessage()).append(",");
            }
            throw new APIException(ResultCode.FAILED, sb.toString());
        }
        if (vo.getId() == null || vo.getId() < 1) {
            throw new APIException(ResultCode.FAILED, "用户Id为空或小于1!");
        }
        return "success";
    }
}
