package com.cz.validate.exception;

import com.cz.validate.common.ResultCode;
import lombok.Data;

/**
 * @Description todo
 * @Author cz
 * @Date 2022/6/26
 */
@Data
public class APIException extends Throwable {
    private Integer code;
    private String msg;

    public APIException(ResultCode resultCode, String s) {
        this.code = resultCode.getCode();
        this.msg = s;
    }
}
