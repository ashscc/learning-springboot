package com.cz.validate.service;

import com.cz.validate.model.ProductInfo;
import com.cz.validate.vo.ProductInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * @Description todo
 * @Author cz
 * @Date 2022/6/23
 */
@Service
public class ProductInfoService {

    public ProductInfoVo getAllByFilter(ProductInfo productInfo) {
        ProductInfoVo vo = new ProductInfoVo();
        BeanUtils.copyProperties(productInfo, vo);
        vo.setProductId(1L);
        return vo;
    }
}
