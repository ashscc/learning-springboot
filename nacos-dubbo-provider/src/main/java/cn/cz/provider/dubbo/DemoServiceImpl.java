package cn.cz.provider.dubbo;


import demo.DemoService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author chengzhen
 * @date 2021/1/29
 * @time 1:52 下午
 */
@DubboService(version = "${demo.service.version}")
public class DemoServiceImpl implements DemoService {

    @Value("${dubbo.application.name}")
    private String serviceName;

    @Override
    public String sayHello(String name) {
        return String.format("[%s] : Hello, %s", serviceName, name);
    }
}
