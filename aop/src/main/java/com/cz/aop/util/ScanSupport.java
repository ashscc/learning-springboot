package com.cz.aop.util;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.lang.NonNull;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ScanSupport implements ResourceLoaderAware {

    public static final ScanSupport scanSupport;
    private ResourceLoader resourceLoader;
    private final ResourcePatternResolver resolver;
    private final MetadataReaderFactory metadataReaderFactory;

    private ScanSupport(){
        resolver = ResourcePatternUtils.getResourcePatternResolver(null);
        metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
    }

    static {
        scanSupport = new ScanSupport();
    }

    /**
     * set注入对象
     */
    @Override
    public void setResourceLoader(@NonNull ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * 利用spring提供的扫描包下面的类信息,再通过classfrom反射获得类信息
     * @param scanPath 扫描包路径
     * @return 类集合
     * @throws IOException 异常
     */
    public Set<Class<?>> doScan(String scanPath) throws IOException {
        Set<Class<?>> classes = new HashSet<>();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
                .concat(ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(scanPath))
                        .concat("/**/*.class"));
        Resource[] resources = resolver.getResources(packageSearchPath);
        MetadataReader metadataReader;
        for (Resource resource : resources) {
            if (resource.isReadable()) {
                metadataReader = metadataReaderFactory.getMetadataReader(resource);
                try {
                    if (metadataReader.getClassMetadata().isConcrete()) {// 当类型不是抽象类或接口在添加到集合
                        classes.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return classes;
    }

    /**
     * 指定包下面的类信息
     *
     * @return 多个类信息
     */
    public static Set<Class<?>> classInfos(String scanPath) throws IOException {
        return scanSupport.doScan(scanPath);
    }
}