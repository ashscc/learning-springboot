package com.cz.aop.facade;

import com.cz.aop.annotation.LabelClass;
import com.cz.aop.annotation.LabelMethod;
import com.cz.aop.util.ScanSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @description TODO
 * @auther chengzhen
 * @date 2022/1/12 11:26
 * Debug the code, debug the world!
 */
@RestController
@RequestMapping("/v1/aop")
public class Controller {

    public static final Logger logger = LoggerFactory.getLogger(Controller.class);
    private static final String FULLTEXT_SCAN_PACKAGE_PATH = "com.cz.aop.server";

    @LabelMethod("测试value")
    @GetMapping(value = "/test/label1")
    public void testAopLabel1() throws IOException {
        logger.info("进入方法label1");
//        tService.testLabelOneInClass("无名");

        ScanSupport.classInfos(FULLTEXT_SCAN_PACKAGE_PATH).forEach(o->{
            if(o != null){
                logger.info("进入的类:{}", o.getName());
                if (o.getAnnotation(LabelClass.class) != null) {
                    logger.info("LabelClass识别到的类:{}", o.getName());
                }
//                try {
//                    Class clz = Class.forName(o.getName());
//                    Method method = MethodUtils.getMatchingAccessibleMethod(clz, "testLabelOneInClass", String.class);
//                    if(method == null) return;
//                    method.invoke(clz.getDeclaredConstructor().newInstance(), "ss");
//                } catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
//                    e.printStackTrace();
//                }
            }
        });
    }

}
