package com.cz.aop.annotation;

import java.lang.annotation.*;

/**
 * @description TODO
 * @auther chengzhen
 * @date 2022/1/13 11:13
 * Debug the code, debug the world!
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LabelClass {
}
