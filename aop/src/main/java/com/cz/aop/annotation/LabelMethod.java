package com.cz.aop.annotation;

import java.lang.annotation.*;

/**
 * @description label注解
 * @auther chengzhen
 * @date 2022/1/12 11:04
 * Debug the code, debug the world!
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LabelMethod {
    String value() default "LabelOne test";
}