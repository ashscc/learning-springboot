package com.cz.aop.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description TODO
 * @auther chengzhen
 * @date 2022/1/12 11:47
 * Debug the code, debug the world!
 */
public class TService {
    public static final Logger logger = LoggerFactory.getLogger(TService.class);

    public void testLabelOneInClass(String name){
        logger.info("方法 testLabelOneInClass 被调用... name:{}", name);
    }
}
