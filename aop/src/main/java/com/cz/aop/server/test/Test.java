package com.cz.aop.server.test;

import com.cz.aop.annotation.LabelClass;

/**
 * @description TODO
 * @auther chengzhen
 * @date 2022/1/12 18:28
 * Debug the code, debug the world!
 */
@LabelClass
public class Test {

    public class TestA {

    }

    @LabelClass
    public static class TestN {

    }
}
