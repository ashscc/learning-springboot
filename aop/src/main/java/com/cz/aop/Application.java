package com.cz.aop;

import com.cz.aop.util.SpringContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringContextUtil.applicationContext =
				SpringApplication.run(Application.class, args);
	}

}
//参考：
//1.https://blog.csdn.net/weixin_45987924/article/details/119254253
//2.https://blog.csdn.net/wsm890325/article/details/93868894
//3.https://www.html.cn/softprog/java/96032.html