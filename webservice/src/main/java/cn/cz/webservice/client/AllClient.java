package cn.cz.webservice.client;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

public class AllClient {

    public static void main(String[] args) {
        client1();
        client2();
        client3();
    }

    // 创建动态客户端
    public static void client1() {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://localhost:8080/Service/common?wsdl");
        Object[] objects = new Object[0];
        try {
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke("common_world", "zhangsan123456");
            System.out.println("返回数据:" + objects[0]);
            client.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 创建动态客户端
    public static void client2() {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://localhost:8080/Service/common?wsdl");
        Object[] objects = new Object[0];
        try {
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke("common_say", "zhangsan123456");
            System.out.println("返回数据:" + objects[0]);
            client.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 创建动态客户端
    public static void client3() {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://localhost:8080/Service/hello?wsdl");
        Object[] objects = new Object[0];
        try {
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke("hello_world", "zhangsan123456");
            System.out.println("返回数据:" + objects[0]);
            client.destroy();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

}