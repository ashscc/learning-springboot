package cn.cz.webservice.service.impl;

import cn.cz.webservice.service.CommonService;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;
/**
 * @author chengzhen
 * @date 2020/8/13
 * @time 10:53 AM
 */
@Component  //一般不会在接口上放注解，在实现类上注解，这样才符合热插拔式，如果后期更换实现类，直接注解其他实现类就可以。
@WebService( serviceName = "CommonService",   //对外发布的服务名
        targetNamespace = "http://service.webservice.cz.cn/",//指定想要的名称空间，通常使用interface接口包名反转
        endpointInterface = "cn.cz.webservice.service.CommonService")//服务接口interface全路径, 服务端点接口
public class CommonServiceImpl implements CommonService {
    @Override
    public String common_world(String name) {
        return "common_world ," + name;
    }

    @WebMethod(exclude=false) //默认fase: 表示发布该方法  true:表示不发布此方法
    @Override
    public String common_say(String name) {
        return "common_say ," + name;
    }
}
