package cn.cz.webservice.service.impl;

import cn.cz.webservice.service.HelloService;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

//@WebService表示该类是一个服务类，需要发布其中的public的方法
@Component
@WebService(serviceName = "HelloService",      // 该webservice服务的名称,与接口中指定的name一致,对外发布的服务名
        targetNamespace = "http://service.webservice.cz.cn1/",   //名称空间，通常使用接口包名反转
        endpointInterface = "cn.cz.webservice.service.HelloService")  //服务接口全路径
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello_world(String myname) {
        //Thread.currentThread().sleep(2000);
        return "hello_world ," + myname;
    }
}