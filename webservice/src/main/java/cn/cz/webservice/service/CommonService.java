package cn.cz.webservice.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author chengzhen
 * @date 2020/8/13
 * @time 10:52 AM
 */


@WebService(name = "CommonService",    // 暴露服务名称
        targetNamespace = "http://service.webservice.cz.cn/")//命名空间,一般是interface的包名倒序
public interface CommonService {
    @WebMethod //标注该方法为webservice暴露的方法,用于向外公布，它修饰的方法是webservice方法，去掉也没影响的，类似一个注释信息。
    @WebResult(name = "String", targetNamespace = "")
    public String common_world(@WebParam(name = "userName") String name);

    //不同的方法
    @WebMethod
    @WebResult(name = "String", targetNamespace = "")
    public String common_say(@WebParam(name = "userName") String name);
}
