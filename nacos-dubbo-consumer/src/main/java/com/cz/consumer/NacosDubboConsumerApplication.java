package com.cz.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NacosDubboConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosDubboConsumerApplication.class, args);
	}

}
