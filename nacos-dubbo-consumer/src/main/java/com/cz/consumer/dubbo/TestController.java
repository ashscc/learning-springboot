package com.cz.consumer.dubbo;

import demo.DemoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author chengzhen
 * @date 2021/1/29
 * @time 2:05 下午
 */
@RestController
public class TestController {

    @DubboReference(version = "${demo.service.version}")
    private DemoService demoService;

    @RequestMapping("/sayHello")
    public String sayHello(@RequestParam String name) {
        return demoService.sayHello(name);
    }
}
