package com.aliyun.openservices.springboot.example.normal;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.aliyun.openservices.springboot.example.config.MqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author chengzhen
 * @date 2020/9/10
 * @time 3:03 PM
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProducerClientTest {

    @Autowired
    private ProducerClient producer;
    @Autowired
    private ConsumerClient consumer;
    @Autowired
    private MqConfig mqConfig;

    @Test
    public void test1(){
        consumer.buildConsumer().start();

//        Message msg = new Message(mqConfig.getTopic(), mqConfig.getTag(), "Hello MQ".getBytes());
//        msg.setKey("唯一的key");
//        SendResult sendResult = producer.buildProducer().send(msg);
//        System.out.println(sendResult);


    }
}