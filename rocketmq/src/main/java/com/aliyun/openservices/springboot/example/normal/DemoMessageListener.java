package com.aliyun.openservices.springboot.example.normal;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import org.springframework.stereotype.Component;

@Component
public class DemoMessageListener implements MessageListener {

    @Override
    public Action consume(Message message, ConsumeContext context) {

        System.out.println("Receive: " + message);
        try {
            //do something..
            String msg = new String(message.getBody(), "utf8");
            if(msg.startsWith("Round")){
                int round = Integer.valueOf(msg.split(":")[1].toString());
                System.out.println("消费者处理的消息，第" + round + "条，且key为" + message.getKey());
            }else{
                System.out.println("消费者消费旧数据:"+msg);
            }
            return Action.CommitMessage;
        } catch (Exception e) {
            //消费失败
            System.out.println("消费失败:"+e.getMessage());
            return Action.ReconsumeLater;
        }
    }
}
