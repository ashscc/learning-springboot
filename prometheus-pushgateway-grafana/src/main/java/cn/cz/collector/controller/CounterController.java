package cn.cz.collector.controller;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author chengzhen
 * @date 2021/3/23
 * @time 6:13 下午
 * application直接接入prometheus
 */
@RestController
@RequestMapping(value = "/v1/api")
@Api(tags = "app2Prometheus", description = "应用推送指标到prometheus")
public class CounterController {

    @Autowired
    private MeterRegistry registry;

    private Counter counter_core;
    private Counter counter_index;
    private AtomicInteger app_online_count;

    @PostConstruct
    private void init(){
        counter_core = registry.counter("app_requests_method_count", "method", "CounterController.core");
        counter_index = registry.counter("app_requests_method_count", "method", "CounterController.index");
        app_online_count = registry.gauge("app_online_count", new AtomicInteger(0));
    }

    @RequestMapping(value = "/index")
    public Object index(){
        counter_index.increment();
        return counter_index.count() + " index of springboot-prometheus.";
    }

    @RequestMapping(value = "/core")
    public Object coreUrl(){
        counter_core.increment();
        return counter_core.count() + " coreUrl Monitor by Prometheus.";
    }

    @RequestMapping(value = "/online")
    public Object onlineCount(){
        int people = 0;
        try {
            people = new Random().nextInt(2000);
            app_online_count.set(people);
        } catch (Exception e){
            return e;
        }
        return "current online people: " + people;
    }

}
