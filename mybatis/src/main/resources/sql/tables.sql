create table if not EXISTS `nice_test`.`app_user` (
    `id` bigint(32) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '递增id',
    `name` VARCHAR(10) NOT NULL COMMENT '名称',
    `age` tinyint(10) NOT NULL COMMENT '年龄'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '用户表';

create table if not EXISTS `role` (
    `id` bigint(32) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '递增id',
    `name` VARCHAR(10) NOT NULL UNIQUE COMMENT '名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '角色表';

create table if not EXISTS `role_authority` (
    `id` bigint(32) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '递增id',
    `role_id` bigint(32) NOT NULL COMMENT '角色id',
    `authority_id` bigint(32) NOT NULL COMMENT '权限id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '角色权限表';

create table if not EXISTS `authority` (
    `id` bigint(32) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '递增id',
    `description` VARCHAR(50) NULL COMMENT '权限描述'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '权限表';