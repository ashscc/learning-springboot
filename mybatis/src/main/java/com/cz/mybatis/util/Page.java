package com.cz.mybatis.util;

/**
 * @description TODO
 * @auther chengzhen
 * @date 2021/9/16 13:28
 * Debug the code, debug the world!
 */
public class Page {
    private Long pageNum;
    private Long pageSize;
    private Long total;
    private Long totalPage;

    public Long getPageNum() {
        return pageNum;
    }

    public void setPageNum(Long pageNum) {
        this.pageNum = pageNum;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }
}
