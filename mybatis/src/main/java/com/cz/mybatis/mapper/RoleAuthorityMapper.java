package com.cz.mybatis.mapper;

import static com.cz.mybatis.mapper.RoleAuthorityDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.cz.mybatis.pojo.RoleAuthority;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface RoleAuthorityMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    BasicColumn[] selectList = BasicColumn.columnList(id, roleId, authorityId);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<RoleAuthority> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<RoleAuthority> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("RoleAuthorityResult")
    Optional<RoleAuthority> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="RoleAuthorityResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="role_id", property="roleId", jdbcType=JdbcType.BIGINT),
        @Result(column="authority_id", property="authorityId", jdbcType=JdbcType.BIGINT)
    })
    List<RoleAuthority> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, roleAuthority, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, roleAuthority, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int deleteByPrimaryKey(Long id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int insert(RoleAuthority record) {
        return MyBatis3Utils.insert(this::insert, record, roleAuthority, c ->
            c.map(id).toProperty("id")
            .map(roleId).toProperty("roleId")
            .map(authorityId).toProperty("authorityId")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int insertMultiple(Collection<RoleAuthority> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, roleAuthority, c ->
            c.map(id).toProperty("id")
            .map(roleId).toProperty("roleId")
            .map(authorityId).toProperty("authorityId")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int insertSelective(RoleAuthority record) {
        return MyBatis3Utils.insert(this::insert, record, roleAuthority, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(roleId).toPropertyWhenPresent("roleId", record::getRoleId)
            .map(authorityId).toPropertyWhenPresent("authorityId", record::getAuthorityId)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default Optional<RoleAuthority> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, roleAuthority, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default List<RoleAuthority> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, roleAuthority, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default List<RoleAuthority> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, roleAuthority, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default Optional<RoleAuthority> selectByPrimaryKey(Long id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, roleAuthority, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    static UpdateDSL<UpdateModel> updateAllColumns(RoleAuthority record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(roleId).equalTo(record::getRoleId)
                .set(authorityId).equalTo(record::getAuthorityId);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(RoleAuthority record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(roleId).equalToWhenPresent(record::getRoleId)
                .set(authorityId).equalToWhenPresent(record::getAuthorityId);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int updateByPrimaryKey(RoleAuthority record) {
        return update(c ->
            c.set(roleId).equalTo(record::getRoleId)
            .set(authorityId).equalTo(record::getAuthorityId)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    default int updateByPrimaryKeySelective(RoleAuthority record) {
        return update(c ->
            c.set(roleId).equalToWhenPresent(record::getRoleId)
            .set(authorityId).equalToWhenPresent(record::getAuthorityId)
            .where(id, isEqualTo(record::getId))
        );
    }
}