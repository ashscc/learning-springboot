package com.cz.mybatis.mapper;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class AuthorityDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: authority")
    public static final Authority authority = new Authority();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.id")
    public static final SqlColumn<Long> id = authority.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.description")
    public static final SqlColumn<String> description = authority.description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: authority")
    public static final class Authority extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<String> description = column("description", JDBCType.VARCHAR);

        public Authority() {
            super("authority");
        }
    }
}