package com.cz.mybatis.mapper;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class RoleDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role")
    public static final Role role = new Role();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role.id")
    public static final SqlColumn<Long> id = role.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role.name")
    public static final SqlColumn<String> name = role.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role")
    public static final class Role extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public Role() {
            super("role");
        }
    }
}