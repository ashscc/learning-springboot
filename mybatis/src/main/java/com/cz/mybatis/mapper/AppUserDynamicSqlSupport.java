package com.cz.mybatis.mapper;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class AppUserDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: app_user")
    public static final AppUser appUser = new AppUser();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.id")
    public static final SqlColumn<Long> id = appUser.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.name")
    public static final SqlColumn<String> name = appUser.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.age")
    public static final SqlColumn<Byte> age = appUser.age;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: app_user")
    public static final class AppUser extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public final SqlColumn<Byte> age = column("age", JDBCType.TINYINT);

        public AppUser() {
            super("app_user");
        }
    }
}