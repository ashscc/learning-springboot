package com.cz.mybatis.mapper;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class RoleAuthorityDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    public static final RoleAuthority roleAuthority = new RoleAuthority();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.id")
    public static final SqlColumn<Long> id = roleAuthority.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.role_id")
    public static final SqlColumn<Long> roleId = roleAuthority.roleId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.authority_id")
    public static final SqlColumn<Long> authorityId = roleAuthority.authorityId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: role_authority")
    public static final class RoleAuthority extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<Long> roleId = column("role_id", JDBCType.BIGINT);

        public final SqlColumn<Long> authorityId = column("authority_id", JDBCType.BIGINT);

        public RoleAuthority() {
            super("role_authority");
        }
    }
}