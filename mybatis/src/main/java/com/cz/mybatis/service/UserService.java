//package com.cz.mybatis.service;
//
//import com.cz.mybatis.pojo.AppUser;
//import com.cz.mybatis.util.Page;
//import org.apache.ibatis.annotations.Param;
//
//import java.util.List;
//
///**
// * @description TODO
// * @auther chengzhen
// * @date 2021/9/16 13:22
// * Debug the code, debug the world!
// */
//public interface UserService {
//
//    /**
//     * 根据用户名称查询表内数据
//     * @param name 用户名
//     * @return 用户
//     */
//    AppUser selectByUserName(@Param("name")String name);
//
//    /**
//     * 分页查找数据
//     * @param page 分页参数
//     * @return 用户列表
//     */
//    List<AppUser> selectAll(Page page);
//}
