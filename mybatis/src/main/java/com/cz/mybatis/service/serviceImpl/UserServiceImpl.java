//package com.cz.mybatis.service.serviceImpl;
//
//import com.cz.mybatis.mapper.AppUserDynamicSqlSupport;
//import com.cz.mybatis.mapper.AppUserMapper;
//import com.cz.mybatis.pojo.AppUser;
//import com.cz.mybatis.service.UserService;
//import com.cz.mybatis.util.Page;
//import org.mybatis.dynamic.sql.SqlBuilder;
//import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.List;
//import java.util.Optional;
//
//import static org.mybatis.dynamic.sql.SqlBuilder.isEqualToWhenPresent;
//import static org.mybatis.dynamic.sql.render.RenderingStrategies.MYBATIS3;
//
///**
// * @description TODO
// * @auther chengzhen
// * @date 2021/9/16 13:22
// * Debug the code, debug the world!
// */
//public class UserServiceImpl implements UserService {
//
//    private final AppUserMapper appUserMapper;
//
//    @Autowired
//    public UserServiceImpl(AppUserMapper appUserMapper) {
//        this.appUserMapper = appUserMapper;
//    }
//
//    @Override
//    public AppUser selectByUserName(String name) {
//        SelectStatementProvider
//                selectStatement = SqlBuilder.select(AppUserMapper.selectList)
//                .from(AppUserDynamicSqlSupport.appUser)
//                .where(AppUserDynamicSqlSupport.name,
//                        isEqualToWhenPresent(("" == name) ? null : name))
//                .orderBy(AppUserDynamicSqlSupport.id)
//                .build()
//                .render(MYBATIS3);
//
//        Optional<AppUser> appUser = appUserMapper.selectOne(selectStatement);
//        return appUser.orElse(null);
//    }
//
//    @Override
//    public List<AppUser> selectAll(Page page) {
//        return null;
//    }
//}
