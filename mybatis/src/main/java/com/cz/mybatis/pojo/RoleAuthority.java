package com.cz.mybatis.pojo;

import javax.annotation.Generated;

public class RoleAuthority {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.role_id")
    private Long roleId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.authority_id")
    private Long authorityId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.role_id")
    public Long getRoleId() {
        return roleId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.role_id")
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.authority_id")
    public Long getAuthorityId() {
        return authorityId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: role_authority.authority_id")
    public void setAuthorityId(Long authorityId) {
        this.authorityId = authorityId;
    }
}