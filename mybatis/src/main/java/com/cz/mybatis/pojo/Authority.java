package com.cz.mybatis.pojo;

import javax.annotation.Generated;

public class Authority {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.description")
    private String description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.description")
    public String getDescription() {
        return description;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: authority.description")
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}