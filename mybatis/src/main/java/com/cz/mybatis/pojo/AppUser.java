package com.cz.mybatis.pojo;

import javax.annotation.Generated;

public class AppUser {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.name")
    private String name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.age")
    private Byte age;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.name")
    public String getName() {
        return name;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.name")
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.age")
    public Byte getAge() {
        return age;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: app_user.age")
    public void setAge(Byte age) {
        this.age = age;
    }
}