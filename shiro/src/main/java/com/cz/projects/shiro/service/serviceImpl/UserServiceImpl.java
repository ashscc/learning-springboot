package com.cz.projects.shiro.service.serviceImpl;

import com.cz.projects.shiro.database.DataSource;
import com.cz.projects.shiro.service.UserService;
import com.cz.projects.shiro.bean.UserBean;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author chengzhen
 * @date 2020/4/17
 * @time 下午3:16
 */
@Component
public class UserServiceImpl implements UserService{
    @Override
    public UserBean getUser(String username) {
        // 没有此用户直接返回null
        if (! DataSource.getData().containsKey(username))
            return null;

        UserBean user = new UserBean();
        Map<String, String> detail = DataSource.getData().get(username);

        user.setUsername(username);
        user.setPassword(detail.get("password"));
        user.setRole(detail.get("role"));
        user.setPermission(detail.get("permission"));
        return user;
    }
}
