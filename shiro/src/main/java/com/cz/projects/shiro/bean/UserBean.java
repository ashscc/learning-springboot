package com.cz.projects.shiro.bean;

import lombok.Data;

/**
 * @author chengzhen
 * @date 2020/4/17
 * @time 下午3:16
 */
@Data
public class UserBean {
    private String username;

    private String password;

    private String role;

    private String permission;
}