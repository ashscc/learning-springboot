package com.cz.projects.shiro.service;

import com.cz.projects.shiro.bean.UserBean;

/**
 * @author chengzhen
 * @date 2020/4/17
 * @time 下午3:15
 */
public interface UserService {
    UserBean getUser(String username);
}
