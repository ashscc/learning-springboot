package com.cz.projects.shiro.exceptions;

/**
 * @author chengzhen
 * @date 2020/4/17
 * @time 下午3:22
 */
public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException(String msg) {
        super(msg);
    }

    public UnauthorizedException() {
        super();
    }
}