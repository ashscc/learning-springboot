package com.cz.projects.shiro.config;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author chengzhen
 * @date 2020/4/17
 * @time 下午3:27
 */
public class JWTToken implements AuthenticationToken {

    // 密钥
    private String token;

    public JWTToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}