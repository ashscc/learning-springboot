package demo;

/**
 * @author chengzhen
 * @date 2021/1/29
 * @time 11:31 上午
 */
public interface DemoService {

    String sayHello(String name);
}

