##Elastic Search探索过程
#### 1. 使用docker下载es
```bash
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.13.1
docker images
# 查看库docker.elastic.co/elasticsearch/elasticsearch镜像的id，修改镜像的tag
docker tag {mageId} elasticsearch
docker run -idt --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch
# 参考地址：https://hub.docker.com/_/elasticsearch; https://www.jianshu.com/p/acbedc3092b5
# 查看地址: http://localhost:9200
# 修改elasticsearch.yml中为如下
http.cors.enabled: true
http.cors.allow-origin: "*"
```
#### 2. 使用docker下载kibana
```bash
docker pull docker.elastic.co/kibana/kibana:7.13.1
docker images
# 查看库docker.elastic.co/kibana/kibana镜像的id，修改镜像的tag
docker tag {mageId} kibana
docker run -idt --name kibana -p 5601:5601 kibana
# 参考地址：https://hub.docker.com/_/kibana
# 查看地址: http://localhost:5601
# 修改kibana.yml中
elasticsearch_hosts: ["http://172.22.192.212:9200"]
```
#### 3. 使用docker下载elasticsearch-head
```bash
docker pull mobz/elasticsearch-head:5-alpine
docker images
# 修改head的tag
docker tag mobz/elasticsearch-head:5-alpine head
docker run --restart=always --name head -idt -p 9100:9100 head
# 参考地址：https://hub.docker.com/r/mobz/elasticsearch-head/tags
# 查看地址：http://localhost:9100
```